FROM docker.io/ruby:2.7-slim-buster

# MailCatcher image for OpenShift Origin

ARG DO_UPGRADE=
ENV MAILCATCHER_VERS=0.8.2 \
    RAILS_ENV=production

LABEL io.k8s.description="MailCatcher Image." \
      io.k8s.display-name="MailCatcher" \
      io.openshift.expose-services="8080:http,1025:smtp" \
      io.openshift.tags="mailcatcher" \
      io.openshift.non-scalable="false" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-mailcatcher" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$MAILCATCHER_VERS"

COPY config/* /

RUN set -x \
    && apt-get update \
    && apt-get install -y --no-install-recommends dumb-init \
    && mkdir -p /usr/share/man/man1 /usr/share/man/man7 \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install MailCatcher Dependencies" \
    && apt-get install -y build-essential sqlite libsqlite3-dev \
	libnss-wrapper \
    && echo "# Install MailCatcher" \
    && gem install mailcatcher -v $MAILCATCHER_VERS \
    && mv /reset-tls.sh /nsswrapper.sh /usr/local/bin/ \
    && echo "# Cleaning Up" \
    && apt-get remove --purge -y build-essential \
    && apt-get purge -y --auto-remove \
	-o APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

USER 1001
ENTRYPOINT ["dumb-init","--","/run-mailcatcher.sh"]
