# k8s Mailcatcher

Mailcatcher image.

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name       |    Description              | Default             |
| :--------------------- | --------------------------- | ------------------- |
|  `SMTP_PORT`           | MailCatcher SMTP Port       | `1025`              |
|  `HTTP_PORT`           | MailCatcher HTTP Port       | `8080`              |
