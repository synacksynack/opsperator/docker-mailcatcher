#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

. /usr/local/bin/nsswrapper.sh

SMTP_PORT=${SMTP_PORT:-1025}
HTTP_PORT=${HTTP_PORT:-8080}

exec mailcatcher --foreground \
    --ip 0.0.0.0 \
    --smtp-port $SMTP_PORT \
    --http-port $HTTP_PORT
