#!/bin/sh

if test "`id -u`" -ne 0; then
    if test -s /tmp/mailcatcher-passwd; then
	echo Skipping nsswrapper setup - already initialized
    else
	echo Setting up nsswrapper mapping `id -u` to mailcatcher
	(
	    cat /etc/group
	    echo "mailcatcher:x:`id -g`:"
	) >/tmp/mailcatcher-group
	(
	    cat /etc/passwd
	    echo "mailcatcher:x:`id -u`:`id -g`:mailcatcher:/mailcatcher:/bin/sh"
	) >/tmp/mailcatcher-passwd
    fi
    export NSS_WRAPPER_PASSWD=/tmp/mailcatcher-passwd
    export NSS_WRAPPER_GROUP=/tmp/mailcatcher-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
